import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { LoginService } from "./../../../services/login.service";
import { EmployeeService } from "./../../services/employee.service";
import { IEmployee } from "./../../models/i-employee";

import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-employee-dashboard",
  templateUrl: "./employee-dashboard.component.html",
  styleUrls: ["./employee-dashboard.component.scss"]
})
export class EmployeeDashboardComponent implements OnInit {
  employees: IEmployee[];
  detailStyle = 'inline';
  constructor(
    private employeeService: EmployeeService,
    private loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute
  ) {}


  ngOnInit() {
    this.route.queryParamMap.subscribe((params: ParamMap) => {
      if (params.has('detailStyle')) {
        this.detailStyle = params.get('detailStyle');
      }
     });
    if (this.loginService.isLoggedIn()) {
      this.employeeService
        .getEmployees()
        .subscribe((employees: IEmployee[]) => {
          this.employees = employees;
        });
    } else {
      this.router.navigate(['/home']);
    }
  }

  onDelete(message: string) {
    this.employeeService.getEmployees().subscribe((employees: IEmployee[]) => {
      this.employees = employees;
      console.log(message);
    });
  }

  onDetailStyleChange(value: string) {
    this.detailStyle = value;
    this.router.navigate(['/employees'], { queryParams: {
      detailStyle: value
    }});

  }
}
