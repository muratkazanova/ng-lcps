import { EmployeeSharedService } from './../../services/employee-shared.service';
import { IAddress } from './../../models/i-address';
import { IEmployee } from './../../models/i-employee';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../../../services/logger.service';

@Component({
  selector: 'app-employee-detail-address-info',
  templateUrl: './employee-detail-address-info.component.html',
  styleUrls: ['./employee-detail-address-info.component.scss']
})
export class EmployeeDetailAddressInfoComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private loggerService: LoggerService,
    public employeeSharedService: EmployeeSharedService
  ) { }

  address: IAddress;
  ngOnInit() {
    this.route.parent.data.subscribe(data => {
      this.address = (data['employee'] as IEmployee).addresses[0];
    });
  }

  compareAddressLine(currentValue: string) {
      if (currentValue !== this.address.addressline1) {
        this.employeeSharedService.isAddressChanged = true;
      } else {
        this.employeeSharedService.isAddressChanged = false;
      }

  }
}
