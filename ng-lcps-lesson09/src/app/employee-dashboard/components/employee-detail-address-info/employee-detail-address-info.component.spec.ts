import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDetailAddressInfoComponent } from './employee-detail-address-info.component';

describe('EmployeeDetailAddressInfoComponent', () => {
  let component: EmployeeDetailAddressInfoComponent;
  let fixture: ComponentFixture<EmployeeDetailAddressInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeDetailAddressInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDetailAddressInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
