import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDetailEditComponent } from './employee-detail-edit.component';

describe('EmployeeDetailEditComponent', () => {
  let component: EmployeeDetailEditComponent;
  let fixture: ComponentFixture<EmployeeDetailEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeDetailEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDetailEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
