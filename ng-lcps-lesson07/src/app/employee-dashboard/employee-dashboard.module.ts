import {  RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeDashboardComponent } from '../employee-dashboard/containers/employee-dashbard/employee-dashboard.component';
import { EmployeeInlineDetailComponent } from '../employee-dashboard/components/employee-inline-detail/employee-inline-detail.component';
import { EmployeeService } from './services/employee.service';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeDetailComponent } from '../employee-dashboard/components/employee-detail/employee-detail.component';
import { EmployeeListResolver } from './services/employee-list-resolver.service';
import { EmployeeDetailBasicInfoComponent } from './components/employee-detail-basic-info/employee-detail-basic-info.component';
import { EmployeeDetailAddressInfoComponent } from './components/employee-detail-address-info/employee-detail-address-info.component';
import { EmployeeResolver } from './services/employee-resolver.service';
import { AuthGuard } from '../services/auth-guard.service';
import { AddressInfoGuard } from './services/address-info-guard';
import { EmployeeSharedService } from './services/employee-shared.service';


const ROUTES: Routes = [
  { path: 'employees', component: EmployeeDashboardComponent, resolve: {employees: EmployeeListResolver}},
  { path: 'employees/:id', component: EmployeeDetailComponent,
    resolve: { employee: EmployeeResolver },
    canActivate: [ AuthGuard],
    children: [
      { path: '', redirectTo: 'basicInfo', pathMatch: 'full'},
      { path: 'basicInfo', component: EmployeeDetailBasicInfoComponent },
      { path: 'addressInfo',
        component: EmployeeDetailAddressInfoComponent,
        canActivate: [AddressInfoGuard],
        canDeactivate: [AddressInfoGuard]
      }
    ]},
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    EmployeeDashboardComponent,
    EmployeeInlineDetailComponent,
    EmployeeDetailComponent,
    EmployeeDetailBasicInfoComponent,
    EmployeeDetailAddressInfoComponent
  ],
  providers: [
    EmployeeService,
    EmployeeListResolver,
    EmployeeResolver,
    AddressInfoGuard,
    EmployeeSharedService
  ],
  exports: [EmployeeDashboardComponent]
})
export class EmployeeDashboardModule { }
