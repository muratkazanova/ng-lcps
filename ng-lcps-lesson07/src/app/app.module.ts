import { LogsModule } from './logs/logs.module';
import { EmployeeDashboardComponent } from './employee-dashboard/containers/employee-dashbard/employee-dashboard.component';
import { EmployeeDashboardModule } from './employee-dashboard/employee-dashboard.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginService } from './services/login.service';
import { LoggerService } from './services/logger.service';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { EmployeeDetailComponent } from './employee-dashboard/components/employee-detail/employee-detail.component';
import { AuthGuard } from './services/auth-guard.service';

const ROUTES: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    EmployeeDashboardModule,
    LogsModule
  ],
  providers: [
    LoginService,
    LoggerService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
