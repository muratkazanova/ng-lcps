import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDetailBasicInfoComponent } from './employee-detail-basic-info.component';

describe('EmployeeDetailBasicInfoComponent', () => {
  let component: EmployeeDetailBasicInfoComponent;
  let fixture: ComponentFixture<EmployeeDetailBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeDetailBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDetailBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
