import { Injectable } from '@angular/core';

@Injectable()
export class LoggerService {

  constructor() { }

  isLogVisible = false;
  messages: string[] = [];
  log(message: string) {
    console.log(message);
    this.messages.unshift(`${message} ${new Date().toLocaleString()}`);
  }
}
