import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInlineDetailComponent } from './employee-inline-detail.component';

describe('EmployeeInlineDetailComponent', () => {
  let component: EmployeeInlineDetailComponent;
  let fixture: ComponentFixture<EmployeeInlineDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInlineDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInlineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
