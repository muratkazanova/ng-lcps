import { LoggerService } from './services/logger.service';
import { IUser } from './models/i-user';
import { Component, OnInit } from '@angular/core';
import { LoginService } from './services/login.service';
import {
  Router, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel
} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  currentUser: IUser;
  loading = true;
  constructor(public loggerService:LoggerService,
    public loginService: LoginService,
    private router: Router){}

  ngOnInit() {
    this.router.events.subscribe(routerEvent => {
      if (routerEvent instanceof NavigationStart) {
        this.loading = true;
      }

      if (
        routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationError ||
        routerEvent instanceof NavigationCancel
      ) {
        console.log('end or cancel');
        if(routerEvent instanceof NavigationEnd) {
          this.loggerService.log(`Routed path: ${JSON.stringify(routerEvent.url)}`);
        }
          this.loading = false;
      }
    });
    this.currentUser = this.loginService.currentUser;
  }

  logOut() {
    this.loginService.logout();
    // navigate to home route.
  }

  onDisplayLogs() {
    this.loggerService.isLogVisible =true;
    this.router.navigate([ {outlets: { logswidget: ['loglist'] } } ]);
  }

  onHideLogs() {
    this.loggerService.isLogVisible =false;
    this.router.navigate([ {outlets: { logswidget: null } } ]);
  }
}
