import { PageNotFoundComponent } from './../../../components/page-not-found/page-not-found.component';
import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators , FormBuilder, AbstractControl, ValidatorFn, FormArray} from '@angular/forms'

function languageSkillValidator(c: AbstractControl): {[key:string]: boolean | null} {
  if (c.value && (c.value.length > 0 && c.value.length <= 3)) {
    return null;
  } else {
    return { 'LanguageSkills': true };
  }
}
function languageSkillValidatorParameterized(min: number, max:number): ValidatorFn {
  return (c: AbstractControl): {[key: string] : boolean | null} => {
    if (c.value && (c.value.length > min && c.value.length <= max)) {
        return null;
      } else {
        return { 'LanguageSkills': true };
      }
  }
}

@Component({
  selector: 'app-employee-detail-edit-v2',
  templateUrl: './employee-detail-edit-v2.component.html',
  styleUrls: ['./employee-detail-edit-v2.component.scss']
})
export class EmployeeDetailEditV2Component implements OnInit {

  employee: any;
  employeeForm: FormGroup;
  data: any;
  isNotificationMethodSelected?: boolean;

  get certificates(): FormArray {
    return <FormArray>this.employeeForm.get("certificates");
  }
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.employeeForm =  this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required]],
      gender: ['male', [Validators.required]],
      age: [18, [Validators.required, Validators.min(18), Validators.max(99)]],
      department: ['', Validators.required],
      phoneType: 'Home',
      phoneNumber: '',
      languageSkills: ['', languageSkillValidator],
      notificationMethods: [null,[Validators.required]],
      emailAddress:['', [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]],
      certificates: this.fb.array([this.buildCertificateGroup()])
    });

    setTimeout(() => {
      this.data = {
        genders: [{ text: "female", value: "f" }, { text: "male", value: "m" }],
        departments: [
          { text: "Human Resources", value: "human resources" },
          { text: "Information Technology", value: "information technology" },
          { text: "Marketing", value: "marketing" },
          { text: "Sales", value: "sales" }
        ],
        languages: [
          { text: "English", value: "english" },
          { text: "Chinese", value: "chinese" },
          { text: "Spanish", value: "spanish" },
          { text: "Russian", value: "russian" },
          { text: "French", value: "french" },
          { text: "German", value: "german" },
          { text: "Turkish", value: "turkish" }
        ],
        phonetypes: [
          { text: "Home", value: "home" },
          { text: "Work", value: "work" },
          { text: "Mobile", value: "mobile" }
        ],
        notificationmethods: [
          { text: "Email", checked: false },
          { text: "Call", checked: false },
          { text: "SMS", checked: false }
        ]
      };
      this.employee = {
        department: "Information Technology",
        phoneType: "mobile",
        languages: ["english", "chinese"],
        notificationmethods: [
          { text: "Email", checked: true },
          { text: "SMS", checked: true }
        ]
      };
      this.employeeForm.patchValue({
        department: this.employee.department,
        phoneType: this.employee.phoneType,
        languageSkills: this.employee.languages
      });
      this.checkSelectedNotificationMethods();
    }, 0);

}

  onSubmit() {
    // console.log(JSON.stringify(this.employeeForm));
  }

  buildCertificateGroup(): FormGroup {
    return this.fb.group({
      certificateName: ['', [Validators.required]],
      certificateDate: ['', [Validators.required]]
    });
  }

  addCertificate(): void {
    this.certificates.push(this.buildCertificateGroup());
  }

  onSetValues() {
    this.employeeForm.setValue({
      firstName: 'Murat',
      lastName: 'Kazanova',
      age: 43
    });
  }

  onPatchValues() {
    this.employeeForm.patchValue({
      firstName: 'Ertan',
      age: 72
    });
  }

  changeCheckbox(notificationmethod, event) {
    const method = this.employee.notificationmethods.find((m: any) => {
      return m.text === notificationmethod.text;
    });

    if (method) {
      method.checked = event.target.checked;
    } else {
      notificationmethod.checked = event.target.checked;
      this.employee.notificationmethods.push(notificationmethod);
    }
    this.checkSelectedNotificationMethods();
  }

  setNotificationValidations() {
    const fmEmailAddress = this.employeeForm.get('emailAddress');
    const fmPhoneNumber = this.employeeForm.get('phoneNumber');

    if ( this.employee.notificationmethods
      .filter(m => m.text === 'Email'  && m.checked).length > 0) {
      fmEmailAddress.setValidators(
          [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]
        );
      } else {
        fmEmailAddress.setValidators(
          [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]
        );
    }
    if ( // SMS and Call checking
        this.employee.notificationmethods
        .filter(m => (m.text === 'SMS' || m.text === 'Call')  && m.checked).length > 0) {  // SMS or Phone
          fmPhoneNumber.setValidators([Validators.required]);
      } else {
        fmPhoneNumber.clearValidators();
      }

    fmPhoneNumber.updateValueAndValidity();
    fmEmailAddress.updateValueAndValidity();
  }

  checkSelectedNotificationMethods() {
    this.isNotificationMethodSelected = this.employee.notificationmethods.filter(
      m => m.checked
    ).length > 0 ? true : null;
    this.setNotificationValidations();
    this.employeeForm.patchValue({
      notificationMethods: this.isNotificationMethodSelected
    });
  }

  checkedStatus(notificationmethod: any) {
    const method = this.employee.notificationmethods.find((m: any) => {
      return m.text === notificationmethod.text;
    });
    if (method) {
      return method.checked;
    } else {
      return false;
    }
  }
}
