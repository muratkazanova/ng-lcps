import { IAddress } from './i-address';
export interface IEmployee {
  id: string;
  firstname: string;
  lastname: string;
  gender: string;
  phonenumber: string;
  emailaddress: string;
  addresses: IAddress[];
}
