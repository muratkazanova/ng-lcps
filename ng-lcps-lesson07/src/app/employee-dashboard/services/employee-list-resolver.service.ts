import { EmployeeService } from './employee.service';
import { IEmployee } from './../models/i-employee';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class EmployeeListResolver implements Resolve<IEmployee[]> {

  constructor(private employeeService: EmployeeService) { }

  resolve(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<IEmployee[]> {
      return this.employeeService.getDelayedEmployees();
    }
}
