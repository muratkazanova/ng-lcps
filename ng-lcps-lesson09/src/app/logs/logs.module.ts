import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogListComponent } from './containers/log-list/log-list.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'loglist', component: LogListComponent, outlet: 'logswidget' }
    ])
  ],
  declarations: [
    LogListComponent
  ]
})
export class LogsModule { }
