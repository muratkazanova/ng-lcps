import { IEmployee } from './../../models/i-employee';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../../../services/logger.service';

@Component({
  selector: 'app-employee-detail-basic-info',
  templateUrl: './employee-detail-basic-info.component.html',
  styleUrls: ['./employee-detail-basic-info.component.scss']
})
export class EmployeeDetailBasicInfoComponent implements OnInit {

  employee: IEmployee;
  constructor(private route: ActivatedRoute, private loggerService: LoggerService) { }

  ngOnInit() {
    this.route.parent.data.subscribe(data => {
      this.employee = data['employee'];
    });
  }

}
