import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDetailEditV2Component } from './employee-detail-edit-v2.component';

describe('EmployeeDetailEditV2Component', () => {
  let component: EmployeeDetailEditV2Component;
  let fixture: ComponentFixture<EmployeeDetailEditV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeDetailEditV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDetailEditV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
