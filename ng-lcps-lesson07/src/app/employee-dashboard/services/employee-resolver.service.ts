import { EmployeeService } from './employee.service';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { IEmployee } from '../models/i-employee';

@Injectable()
export class EmployeeResolver implements Resolve<IEmployee> {

  constructor(private employeeService: EmployeeService) { }

  resolve(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<IEmployee> {
      const id = route.paramMap.get('id');
      return this.employeeService.getEmployee(id);
  }
}
