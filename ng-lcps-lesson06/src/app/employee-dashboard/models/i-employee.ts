export interface IEmployee {
  id: string;
  firstname: string;
  lastname: string;
  gender: string;
  phonenumber: string;
  emailaddress: string;
}
