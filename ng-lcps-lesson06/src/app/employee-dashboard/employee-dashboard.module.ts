import {  RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeDashboardComponent } from '../employee-dashboard/containers/employee-dashbard/employee-dashboard.component';
import { EmployeeInlineDetailComponent } from '../employee-dashboard/components/employee-inline-detail/employee-inline-detail.component';
import { EmployeeService } from './services/employee.service';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeDetailComponent } from '../employee-dashboard/components/employee-detail/employee-detail.component';

const ROUTES: Routes = [];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [
    EmployeeDashboardComponent,
    EmployeeInlineDetailComponent,
    EmployeeDetailComponent
  ],
  providers: [EmployeeService],
  exports: [EmployeeDashboardComponent]
})
export class EmployeeDashboardModule { }
