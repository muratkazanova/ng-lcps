import { EmployeeSharedService } from './employee-shared.service';
import { EmployeeDetailAddressInfoComponent } from './../components/employee-detail-address-info/employee-detail-address-info.component';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanDeactivate } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { LoggerService } from '../../services/logger.service';

@Injectable()
export class AddressInfoGuard implements CanActivate,
CanDeactivate<EmployeeDetailAddressInfoComponent> {

  constructor(
    private loginService: LoginService,
    private loggerService: LoggerService,
    private employeeSharedService: EmployeeSharedService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.loginService.isLoggedIn() && this.loginService.currentUser.userName === 'admin') {
      this.loggerService.log('AddressInfoGuard is applied, user is authorized to see address info.');
      return true;
    } else {
      this.loggerService.log('AddressInfoGuard is applied, user is not authorized to see address info.');
      return false;
    }
  }

  canDeactivate(component: EmployeeDetailAddressInfoComponent,
    route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.employeeSharedService.isAddressChanged) {
        alert(`Address changed, you can't navigate`);
        return false;
      } else {
        return true;
      }
    }
}
