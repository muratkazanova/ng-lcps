import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { IEmployee } from './../../models/i-employee';
import { EmployeeService } from './../../services/employee.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {

  employee: IEmployee;
  constructor(private route: ActivatedRoute, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.route.paramMap.subscribe (params => {
      this.getEmployee(params.get('id'));
    });
  }

  getEmployee(employeeId: string) {
    this.employeeService.getEmployee(employeeId).subscribe( (emp: IEmployee) => {
      this.employee = emp;
    });
  }
}

