import { Observable } from 'rxjs/Observable';
import { IEmployee } from './../../models/i-employee';
import { EmployeeService } from './../../services/employee.service';
import { Component, Input, Output, OnInit, EventEmitter,  OnChanges, SimpleChanges } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employee-inline-detail',
  templateUrl: './employee-inline-detail.component.html',
  styleUrls: ['./employee-inline-detail.component.scss']
})
export class EmployeeInlineDetailComponent implements OnInit, OnChanges {

  @Input() employee: IEmployee;
  @Input() detailstyle: string;
  @Output() Delete: EventEmitter<string> = new EventEmitter<string>();
  isEditing = false;
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.employee) {
      this.employee = Object.assign({}, changes.employee.currentValue);
    }
  }
  onEdit() {
    if (this.detailstyle === 'inline') {
      this.isEditing = true;
    } else {
      this.router.navigate(['/employees', this.employee.id], { queryParams: {
        detailStyle: this.detailstyle
      }
    });
  }
  }
  onUpdate() {
    const { firstname, lastname, emailaddress } = this.employee;
    this.employeeService.updateEmployee(this.employee).subscribe(
      (val:IEmployee) => {
      this.isEditing = false;
      console.log({firstname, lastname, emailaddress}); },
      (error: HttpErrorResponse) => {
        alert(`Updating Employee with an Employee Id: ${this.employee.id} failed  with ${error.message}`);
      }
    );
  }

  onDelete(id: string) {
    this.employeeService.deleteEmployee(id).subscribe(val => {
        this.Delete.emit(`Employee with an id ${id} deleted`);
    },
    (error: HttpErrorResponse) => {
      alert(`Error: ${error.message}`);
    }
  );
  }
}
