import { IEmployee } from './../models/i-employee';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { map } from 'rxjs/operators';

import 'rxjs/add/operator/delay';

@Injectable()
export class EmployeeService {

  baseUrl = 'http://localhost:3000/api/employees/';
  constructor(private http: HttpClient) {}

  getEmployees(): Observable<IEmployee[]> {
    return this.http.get<IEmployee[]>(this.baseUrl);
  }

  getDelayedEmployees(): Observable<IEmployee[]> {
    return this.http.get<IEmployee[]>(this.baseUrl).delay(2000);
  }
  getEmployee(employeeId: string): Observable<IEmployee> {
    return this.http.get<IEmployee>(`${this.baseUrl}/${employeeId}?_embed=addresses`);
  }
  updateEmployee(employee: IEmployee): Observable<any> {
    return this.http.put<IEmployee>(
      `${this.baseUrl}/${employee.id}`, employee);
  }
  deleteEmployee(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

}
