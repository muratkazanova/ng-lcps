export interface IAddress {
  addressline1: string;
  addressline2: string;
  city: string;
  stateprovince: string;
  postalcode: string;
  country: string;
}
