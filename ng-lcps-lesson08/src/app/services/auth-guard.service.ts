import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoginService } from './login.service';
import { LoggerService } from './logger.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private loginService: LoginService, 
    private loggerService: LoggerService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if(this.loginService.isLoggedIn()) {
      this.loggerService.log('AuthGuard applied, user is authorized to navigate to the route.');
      return true;
    }
    else {
      this.loggerService.log('AuthGuard applied, user is not authorized to navigate to the route.')
      this.router.navigate(['/login']);
      return false;
    }
    
  }

}
