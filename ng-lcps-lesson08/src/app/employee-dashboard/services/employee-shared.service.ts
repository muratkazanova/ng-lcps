import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class EmployeeSharedService {

  isAddressChanged = false;
  isAddressInfoEditMode = false;
  constructor(private router: Router) { }

  get isAddressInfoRoute() {
    if (this.router.routerState.snapshot.url.indexOf('addressInfo') !== -1) {
      return true;
    }
    else {
      this.isAddressInfoEditMode = false;
      this.isAddressChanged = false;
      return false;
    }
  }
}
