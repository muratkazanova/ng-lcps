import { IUser } from './models/i-user';
import { Component, OnInit } from '@angular/core';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  currentUser: IUser;
  constructor(public loginService: LoginService){}

  ngOnInit() {
    this.currentUser = this.loginService.currentUser;
  }

  logOut() {
    this.loginService.logout();
    // navigate to home route.
  }
}
