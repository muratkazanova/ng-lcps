import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-employee-detail-edit",
  templateUrl: "./employee-detail-edit.component.html",
  styleUrls: ["./employee-detail-edit.component.scss"]
})
export class EmployeeDetailEditComponent implements OnInit {
  employee: any;
  data: any;
  isNotificationMethodSelected?: boolean;
  @ViewChild('employeeForm') employeeForm: NgForm;
  constructor() {}

  ngOnInit() {


    setTimeout(() => {
      this.data = {
        genders: [{ text: "female", value: "f" }, { text: "male", value: "m" }],
        departments: [
          { text: "Human Resources", value: "human resources" },
          { text: "Information Technology", value: "information technology" },
          { text: "Marketing", value: "marketing" },
          { text: "Sales", value: "sales" }
        ],
        languages: [
          { text: "English", value: "english" },
          { text: "Chinese", value: "chinese" },
          { text: "Spanish", value: "spanish" },
          { text: "Russian", value: "russian" },
          { text: "French", value: "french" },
          { text: "German", value: "german" },
          { text: "Turkish", value: "turkish" }
        ],
        phonetypes: [
          { text: "Home", value: "home" },
          { text: "Work", value: "work" },
          { text: "Mobile", value: "mobile" }
        ],
        notificationmethods: [
          { text: "Email", checked: false },
          { text: "Call", checked: false },
          { text: "SMS", checked: false }
        ]
      };
      this.employee = {
        department: "",
        phonetype: "mobile",
        languages: ["english", "chinese"],
        notificationmethods: [
          { text: "Email", checked: true },
          { text: "SMS", checked: true }
        ]
      };
      this.checkSelectedNotificationMethods();
    }, 100);
  }

  changeCheckbox(notificationmethod, event) {
    const method = this.employee.notificationmethods.find((m: any) => {
      return m.text === notificationmethod.text;
    });

    if (method) {
      method.checked = event.target.checked;
    } else {
      notificationmethod.checked = event.target.checked;
      this.employee.notificationmethods.push(notificationmethod);
    }

    this.checkSelectedNotificationMethods();
  }

  checkSelectedNotificationMethods() {
    this.isNotificationMethodSelected = this.employee.notificationmethods.filter(
      m => m.checked
    ).length > 0 ? true : null;
  }

  checkedStatus(notificationmethod: any) {
    const method = this.employee.notificationmethods.find((m: any) => {
      return m.text === notificationmethod.text;
    });
    if (method) {
      return method.checked;
    } else {
      return false;
    }
  }
  saveEmployee(employeeForm: NgForm) {
    console.log(employeeForm.value);
    console.log(
      employeeForm.value.notificationmethods
        .filter(m => m.checked)
        .map(m => m.text)
    );
  }
}
