import { Router } from '@angular/router';
import { IUser } from "./../models/i-user";
import { LoggerService } from "./logger.service";
import { Injectable } from "@angular/core";

@Injectable()
export class LoginService {
  currentUser: IUser;
  constructor(private loggerService: LoggerService, private router: Router) {}

  isLoggedIn(): boolean {
    return !!this.currentUser;
  }
  login(userName: string, password: string): boolean {
    if (userName === "admin" && password === "pwd!2018") {
      this.currentUser = {
        id: 1,
        userName: userName,
        isAdmin: true
      };
      this.loggerService.log("Admin login");
      return true;
    } else if (
      userName.toLowerCase() === "intro2nguser" &&
      password === "pwd!2018"
    )  {
      this.currentUser = {
        id: 2,
        userName: userName,
        isAdmin: false
      };
      this.loggerService.log(`User ${userName} successfully signed in.`);
      return true;
    } else {
      this.loggerService.log(`Failed to sign in.`);
      return false;
    }
  }

  logout(): void {
    this.currentUser = null;
    this.loggerService.log('User signed out..');
    this.router.navigate(['/home']);
  }
}
