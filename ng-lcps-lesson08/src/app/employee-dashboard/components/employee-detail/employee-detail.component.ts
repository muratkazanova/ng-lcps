import { EmployeeSharedService } from './../../services/employee-shared.service';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { IEmployee } from './../../models/i-employee';
import { EmployeeService } from './../../services/employee.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {

  employee: IEmployee;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeService: EmployeeService,
    public employeeSharedService: EmployeeSharedService
  ) { }

  ngOnInit() {}

  onAddressClick() {
    this.router.navigate(['addressInfo'], {relativeTo: this.route, queryParamsHandling: 'preserve'});
  }

  onAddressInfoEdit() {
    this.employeeSharedService.isAddressInfoEditMode = true;
  }

}

