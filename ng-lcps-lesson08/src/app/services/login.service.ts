import { Router } from '@angular/router';
import { IUser } from "./../models/i-user";
import { LoggerService } from "./logger.service";
import { Injectable } from "@angular/core";

@Injectable()
export class LoginService {
  currentUser: IUser;
  constructor(private loggerService: LoggerService, private router: Router) {}

  isLoggedIn(): boolean {
    const storedUser = sessionStorage.getItem('currentuser');
    if (storedUser) {
      try {
        this.currentUser = JSON.parse(storedUser);
      } catch(e) {
        this.currentUser = null;
      }
    }
    return !!this.currentUser;
  }
  login(userName: string, password: string): boolean {
    if (userName === "admin" && password === "pwd!2018") {
      this.currentUser = {
        id: 1,
        userName: userName,
        isAdmin: true
      };
      this.loggerService.log("Admin login");
      console.log(JSON.stringify(this.currentUser));
      sessionStorage.setItem('currentuser', JSON.stringify(this.currentUser));
      return true;
    } else if (
      userName.toLowerCase() === "intro2nguser" &&
      password === "pwd!2018"
    )  {
      this.currentUser = {
        id: 2,
        userName: userName,
        isAdmin: false
      };
      sessionStorage.setItem('currentuser', JSON.stringify(this.currentUser));
      this.loggerService.log(`User ${userName} successfully signed in.`);
      return true;
    } else {
      this.loggerService.log(`Failed to sign in.`);
      return false;
    }
  }

  logout(): void {
    this.currentUser = null;
    sessionStorage.removeItem('currentuser');
    this.loggerService.log('User signed out..');
    this.router.navigate(['/home']);
  }
}
