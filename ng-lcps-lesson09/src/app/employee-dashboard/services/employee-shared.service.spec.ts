import { TestBed, inject } from '@angular/core/testing';

import { EmployeeSharedService } from './employee-shared.service';

describe('EmployeeSharedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeSharedService]
    });
  });

  it('should be created', inject([EmployeeSharedService], (service: EmployeeSharedService) => {
    expect(service).toBeTruthy();
  }));
});
